import os
import subprocess
from conf.model_ports import model_ports

def get_compile_args(type: str, file_name):

    # Default gcc
    if type == "default_gcc":
        return ["gcc", "./generated/" + file_name, "-o", "./generated/" + file_name]
    # Intrinsic gcc
    if type == "intrinsic_gcc":
        return ["gcc", "-mavx2", "./generated/" + file_name, "-o", "./generated/" + file_name]
    
def compile(compile_type: str, filename: str):
    try:
        compiled = subprocess.run(get_compile_args(compile_type, filename), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if compiled.returncode != 0:
            return False
    except:
        return False
    return True

def write(filename: str, content: str):
    try:
        with open("./generated/" + filename, 'w') as file:
            file.write(content)
    except:
        return False
    return True

def init():
    try:
        os.makedirs("./generated", exist_ok=True)
    except:
        return False
    return True