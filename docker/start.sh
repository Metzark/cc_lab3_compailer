#!/bin/bash
# 1 argument required for the model name

ollama serve & sleep 10  

echo $1

ollama pull $1

ollama run $1

tail -f /dev/null
